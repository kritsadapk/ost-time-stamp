export default {
  fullDMY: (local) => {
    if (local === 'th') {
      return 'DD MMMM xkY'
    }
    return 'DD MMMM YYYY'
  },
  LLLL: (local) => {
    return 'DD MMMM YYYY'
  }
}
