import Vue from 'vue'
import moment from 'moment'
import ls from 'local-storage'
import EventBus from 'eventbusjs'
import conf from './config/config'
import df from './dateformat'
var locale = 'th'

EventBus.addEventListener('TRIGGER_DATE', () => {
  locale = ls.get('locale') || conf.set.DEFAULT_LANGUAGE
  moment.locale(locale)
  console.log(locale)
  if (locale === 'th_TH') {
    require('moment-thai-solar-calendar')
  }
})

Vue.filter('upper', (text) => {
  if (typeof text !== 'string') {
    return ''
  }
  return text.toUpperCase()
})
Vue.filter('formatDate', (value) => {
  if (value) {
    return moment().format(df.fullDMY(locale))
  }
})

Vue.filter('formatTime', (value) => {
  if (value) {
    return (moment(value).format('hh:mm:ss')) // hh:mm:ss a : ก่อนเที่ยง
  }
})

Vue.filter('formatDateTime', (value) => {
  if (value) {
    return (moment(value).format('LLLL'))
  }
})
