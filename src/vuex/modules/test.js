let data = [
  {id: 1, fname: 'Panuwat', lname: 'Salata', position: 'Web dev'},
  {id: 3, fname: 'Songkran', lname: 'Suksirisaysorn', position: 'Manger'}
]

const mutations = {
  increment (state) { // รับ callback เป็น state เสมอ
    state.counter += 1 // ใช้หลักการ reactivity ไม่มีการ return ค่าใดๆ
  },
  SET_USER_LIST: (state, {list}) => {
    state.users = list
  },
  ADD_USER: (state, {user}) => {
    state.users.push(user)
  },
  SET_CURRENT_USER: (state, {user}) => {
    state.currentUser = user
  }
}

const actions = {
  LOAD_USER_LIST: ({commit}) => {
    setTimeout(function () { // connect api
      commit('SET_USER_LIST', { list: data })
    }, 300)
  },
  ADD_NEW_USER: ({commit}) => {
    commit('ADD_USER', {user: {id: state.users.length + 1, fname: 'Testuser', lname: 'Alibaba', position: 'SA'}})
  },
  LOAD_CURRENT_USER: ({commit}) => {
    commit('SET_CURRENT_USER', {user: {id: 1, name: 'คุณทักศิณ ชินวัตรนาการน์', position: 'Web Developer'}})
  }
}

const getters = {
  lengthOfUsers: state => {
    return state.users.length
  }
}

const state = {
  counter: 1,
  currentUser: {},
  users: []
}

export default {
  actions,
  getters,
  state,
  mutations
}
