import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'
import EventBus from 'eventbusjs'
import ls from 'local-storage'
import { sync } from 'vuex-router-sync'

import plugins from './config/plugins'
import configRouter from './config/router'
import messages from './config/languages'
import guards from './config/guards'
import conf from './config/config'
import vuexStore from './vuex/store'

// import store from './vuex/store'

import 'vue-material/dist/vue-material.css'

import App from './App'
import './filter'
import './assets/style.scss'

EventBus.dispatch('TRIGGER_DATE')
Vue.config.productionTip = false

const router = configRouter(VueRouter)
plugins(Vue, VueI18n, Vuex, VueRouter)

const store = new Vuex.Store(vuexStore)
guards(router)
sync(store, router)

/* eslint-disable no-new */
const i18n = new VueI18n({
  locale: ls.get('locale') || conf.set.DEFAULT_LANGUAGE,
  messages
})

new Vue({
  router,
  store,
  i18n,
  el: '#app',
  render: h => h(App)
}).$mount('#app')
